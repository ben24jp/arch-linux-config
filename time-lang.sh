#!/usr/bin/bsh

# Seting timezone
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime

# Setting hardware clock
hwclock --systohc --utc

# Generating the locale for language
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
