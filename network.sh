#!/usr/bin/bash

systemctl enable dhcpcd@wlp2s0.service

pacman -S wireless_tools wpa_supplicant wpa_actiond dialogi

wifi-menu

systemctl enable net-auto-wireless.service
