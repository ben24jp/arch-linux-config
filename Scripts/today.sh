#!/bin/bash 

DWEEK=$(date +'%a')
MDATE=$(date +'%b %e')
YEAR=$(date +'%Y')

echo "${DWEEK} ${MDATE} ${YEAR}"

exit 0
