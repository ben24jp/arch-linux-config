#!/bin/bash

BAT=$(acpi -b | cut -d " " -f 4)
COND=$(acpi -b | cut -d " " -f 3)
COND=$(echo "${COND::-1}")
REM=$(acpi -b | cut -d " " -f 5)
if [ "${COND}" == "Discharging" ]; then
	echo "Battery:${BAT::-1} ${COND} Remaining:${REM}"
else
	echo "Battery:${BAT} Charging"
fi
exit 0
